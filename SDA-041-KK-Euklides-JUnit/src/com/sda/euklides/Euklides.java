package com.sda.euklides;

import java.util.Arrays;

public class Euklides {

	public static void main(String[] args) throws Exception {

		checkNumbers(10, 10);
		int[] myArray = { 1, 2, 3, 4, 5 };
		checkArray(myArray);

	}

	public static int checkNumbers(int a, int b) {

		while (a != b) {

			if (a > b) {
				a = a - b;
			} else {
				b = b - a;

			}

		}
		// dlatego returnuje a bo na koncu i tak to bedzie ta sama liczba czyli
		// dzielnik
		System.out.println("Greates common divider of two given numbers: " + a);
		System.out.println();
		return a;
	}

	public static int checkArray(int[] array) throws Exception {

		if (array.length != 5) {
			throw new Exception("Array needs to be constructed of 5 elements.");
		}

		Arrays.sort(array);
		for (int i = 0; i < array.length; i++) {

			System.out.println("Element of array number " + i + ": " + array[i]);

		}

		while (array[array.length - 1] != array[0]) {

			array[array.length - 1] = array[array.length - 1] - array[0];
			Arrays.sort(array);
		}
		// nie ma znaczenia co zwracamy bo to sie wyr�wna bo jest sortowanie
		System.out.println("Greatest common divider of numbers in array: " + array[0]);
		return array[0];
	}

}
