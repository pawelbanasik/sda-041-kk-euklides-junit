package com.sda.euklides;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class EuklidesTest {

	@Test
	public void checkNumbersTest() {

		assertTrue(Euklides.checkNumbers(5, 10) == 5);
		assertTrue(Euklides.checkNumbers(10, 10) == 10);
		assertFalse(Euklides.checkNumbers(20, 10) == 9);
	}

	@Test
	public void checkArrayTest() throws Exception {

		int[] myArray1 = { 10, 2, 4, 6, 8 };
		assertTrue(Euklides.checkArray(myArray1) == 2);

		int[] myArray2 = { 3, 5, 1, 4, 11 };
		assertFalse(Euklides.checkArray(myArray2) == 0);

		int[] myArray3 = { 2, 6, 8, 10 };
		assertTrue(Euklides.checkArray(myArray3) == 2);

	}
}
